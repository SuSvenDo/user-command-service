package de.dataenv.user.command.impl;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findOneByGoogleSub(String googleSub);

    Long deleteByGoogleSub(String googleSub);
}