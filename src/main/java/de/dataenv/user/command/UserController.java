package de.dataenv.user.command;

import de.dataenv.user.command.impl.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @DeleteMapping("/me")
    public void delete(@AuthenticationPrincipal Jwt jwt){
        var sub = userRepository.deleteByGoogleSub(jwt.getSubject());
    }
}
