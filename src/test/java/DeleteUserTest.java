import de.dataenv.user.command.UserController;
import de.dataenv.user.command.impl.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;


@SpringBootTest(classes = {UserController.class})
@AutoConfigureWebMvc
@AutoConfigureMockMvc
public class DeleteUserTest {

    @MockBean
    UserRepository userRepository;
    @Captor
    ArgumentCaptor<String> subjectCaptor;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void delete_user() throws Exception {
        var delete = delete("/api/user/me");
        delete.with(jwt().jwt(builder -> builder.claim("sub", "subValue")));
        mockMvc.perform(delete).andExpect(status().isOk());
        verify(userRepository).deleteByGoogleSub(subjectCaptor.capture());
        Assertions.assertThat(subjectCaptor.getValue()).isEqualTo("subValue");
    }
}
