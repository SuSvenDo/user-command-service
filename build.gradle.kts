plugins {
    java
    id("org.springframework.boot") version "3.0.4"
    jacoco
}

group = "de.dataenv.user.command"
version = "0.0.1"

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/46554163/packages/maven")
}


dependencies {
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server:3.1.5")
    implementation("org.springframework.boot:spring-boot-starter-web:3.0.6")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb:3.1.5")


    testImplementation("org.springframework.boot:spring-boot-starter-test:3.1.5")
    testImplementation("org.springframework.security:spring-security-test:6.1.5")
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {

    reports {
        csv.required.set(true)
        csv.outputLocation.set(layout.buildDirectory.dir("jacocoCsv").get().asFile)
    }
}
